package 
{
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Jaiko
	 */
	public class En extends Sprite 
	{
		public static const RADIUS:Number = 50;
		private var _en:Sprite;
		public function En() 
		{
			super();
			if (stage) init(null);
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		public function enterFrame():void 
		{
			
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			//
			layout();
		}
		
		private function layout():void 
		{

			_en = new Sprite();
			addChild(_en);
			_en.x = 200;
			_en.y = 200;
			var g:Graphics = _en.graphics;
			g.beginFill(0x0, 0);
			g.lineStyle(1, 0xFF0000);
			g.drawCircle(0, 0, RADIUS);
			
			_en.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
		}
		
		private function mouseDownHandler(e:MouseEvent):void 
		{	
			_en.startDrag();
			stage.addEventListener(MouseEvent.MOUSE_UP , mouseUpHandler);
		}
		
		private function mouseUpHandler(e:MouseEvent):void 
		{
			_en.stopDrag();
			stage.removeEventListener(MouseEvent.MOUSE_UP , mouseUpHandler);
		}
		
		public function get en():Sprite 
		{
			return _en;
		}
	}
}