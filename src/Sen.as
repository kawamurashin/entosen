package 
{
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author Jaiko
	 */
	public class Sen extends Sprite 
	{
		private var _pointList:Array;
		private var dragPoint:Sprite;
		
		public function Sen() 
		{
			super();
			if (stage) init(null);
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		public function enterFrame():void 
		{
			draw();
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			//
			layout();
		}
		
		private function layout():void 
		{
			var i:uint;
			var n:uint;
			var g:Graphics;
			var color:uint = 0x000000;
			var point:Sprite;

			//
			
			_pointList = [];
			n = 2;
			for (i = 0; i < n; i++)
			{
				point = new Sprite();
				addChild(point);
				point.x = 100 +  (stage.stageWidth - 200) * Math.random()
				point.y = 100 +  (stage.stageHeight - 200) * Math.random();
				g = point.graphics;
				g.beginFill(color);
				g.drawCircle(0, 0, 10);
				
				point.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
				
				_pointList.push(point);
			}
			
		}
		
		private function mouseDownHandler(e:MouseEvent):void 
		{
			dragPoint = Sprite(e.currentTarget);
			dragPoint.startDrag(false, new Rectangle(0, 0, 465, 465));
			stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
			addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
		}
		
		private function mouseMoveHandler(e:MouseEvent):void 
		{
			draw();
		}
		
		private function mouseUpHandler(e:MouseEvent):void 
		{
			dragPoint.stopDrag();
			stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
		}
		private function draw():void
		{
			var i:uint;
			var n:uint;
			var point:Sprite;
			var g:Graphics;
			//
			g = this.graphics;
			g.clear();
			
			g.lineStyle(1, 0);
			g.moveTo(_pointList[0].x, _pointList[0].y);
			g.lineTo(_pointList[1].x, _pointList[1].y);
			
		}
		
		public function get pointList():Array 
		{
			return _pointList;
		}
		
	}

}