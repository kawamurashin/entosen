package
{
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author Jaiko
	 */
	public class Main extends Sprite 
	{
		private var sen:Sen;
		private var en:En;
		
		public function Main() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			en = new En();
			addChild(en);
			
			sen = new Sen();
			addChild(sen);
			
			addEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}
		
		private function enterFrameHandler(e:Event):void 
		{
			en.enterFrame();
			sen.enterFrame();
			//
			calculate();
		}
		
		private function calculate():void 
		{
			var d:Number;
			var pointList:Array = sen.pointList;
			var circle:Sprite = en.en;
			var vectorSen:Point = new Point(pointList[1].x - pointList[0].x , pointList[1].y - pointList[0].y);
			var vectorEn:Point = new Point(circle.x - pointList[0].x , circle.y - pointList[0].y);
			
			var dotProduct:Number = vectorSen.x + vectorEn.x + vectorSen.y * vectorEn.y;
			var crossProduct:Number;
			
			var g:Graphics;
			g = this.graphics
			g.clear();
			
			
			g.lineStyle(2, 0x00ff00);
			g.moveTo(pointList[0].x, pointList[0].y);
			g.lineTo(pointList[0].x + vectorEn.x , pointList[0].y + vectorEn.y);
			
			
			var a:Number = pointList[1].y - pointList[0].y;
			var b:Number = pointList[0].x - pointList[1].x;
			var c:Number = -1 * ( (pointList[0].y * b) + (pointList[0].x * a));
			var _x:Number;
			var _y:Number;
			
			var theta:Number;
			var cos:Number;
			
			d = Math.abs(a * circle.x + b * circle.y +c) / Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
			crossProduct = vectorSen.x * vectorEn.y - vectorSen.y * vectorEn.x;
			if (crossProduct < 0)
			{
				theta = Math.atan2(vectorSen.x, -1*vectorSen.y);
			}
			else
			{
				theta = Math.atan2(-1*vectorSen.x, vectorSen.y);
			}
			_x = circle.x + d * Math.cos(theta);
			_y = circle.y + d * Math.sin(theta);
			g.moveTo(circle.x, circle.y);
			g.lineTo(_x, _y);
			g.lineTo(pointList[0].x, pointList[0].y);
			g.endFill();
			
			g = this.graphics
			g.lineStyle();
			g.beginFill(0x0000FF);
			g.drawCircle( _x, _y, 3);
			
			if (crossProduct < 0)
			{
				theta = Math.atan2(vectorSen.x, -1*vectorSen.y);
			}
			else
			{
				theta = Math.atan2(-1*vectorSen.x, vectorSen.y);
			}
			if (d > En.RADIUS)
			{
				//接しません
			}
			else if (d == En.RADIUS)
			{
				//接します
			}
			else
			{
				//交差します
				cos = Math.acos(d / En.RADIUS);
				g = this.graphics
				g.lineStyle();
				g.beginFill(0x00ff00);
				g.drawCircle(circle.x + En.RADIUS * Math.cos(theta + cos), circle.y + En.RADIUS * Math.sin(theta + cos), 5);
				g.drawCircle(circle.x + En.RADIUS * Math.cos(theta - cos), circle.y + En.RADIUS * Math.sin(theta - cos), 5);
				g.endFill();
			}
		}
		
	}
	
}